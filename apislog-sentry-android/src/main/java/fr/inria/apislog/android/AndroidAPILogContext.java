/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apislog.android;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import fr.inria.apislog.APILogContext;
import fr.inria.apislog.APISLog;

/**
 * @author Nicolas Haderer
 * 
 */
public class AndroidAPILogContext implements APILogContext {
	private final Context context;
	private final String sentryDns;
	private final String packageName;

	public AndroidAPILogContext(final Context context) {
		this.context = context;
		this.packageName = context.getPackageName();

		int urlId = context.getResources().getIdentifier("apislog", "string",
				context.getPackageName());
		if (urlId > 0)
			this.sentryDns = context.getResources().getString(urlId);
		else
			this.sentryDns = System.getProperty(APISLog.PROPERTY_BACKEND_URL,
					null);
	}

	@Override
	public String getSentryDns() {
		return this.sentryDns;
	}

	@Override
	public String getLoggerName() {
		return this.packageName;
	}

	@Override
	public Boolean hasNetworkConnection() {
		final ConnectivityManager conMgr = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (conMgr != null) {
			NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
			if (netInfo != null) {
				if (!netInfo.isConnected())
					return false;
				if (!netInfo.isAvailable())
					return false;
			} else
				return false;

		} else
			return false;

		return true;
	}

	@Override
	public Map<String, String> getCommunTags() {
		final Map<String, String> tags = new HashMap<String, String>();

		tags.put("hardware", android.os.Build.HARDWARE);
		tags.put("sdk-int", String.valueOf(android.os.Build.VERSION.SDK_INT));
		tags.put("sdk-release",
				String.valueOf(android.os.Build.VERSION.RELEASE));
		tags.put("manufacturer", String.valueOf(android.os.Build.MANUFACTURER));
		tags.put("device-model", getDeviceName());

		final Runtime runtime = Runtime.getRuntime();
		tags.put("availableProcessors",
				String.valueOf(runtime.availableProcessors()));
		tags.put("maxMemory", String.valueOf(runtime.maxMemory()));

		try {
			tags.put(
					"app-version",
					context.getPackageManager().getPackageInfo(
							context.getPackageName(), 0).versionName);

		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

		return tags;
	}

	private String getDeviceName() {
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (model.startsWith(manufacturer))
			return capitalize(model);
		else
			return capitalize(manufacturer) + " " + model;
	}

	private String capitalize(String s) {
		if (s == null || s.length() == 0)
			return "";
		char first = s.charAt(0);
		if (Character.isUpperCase(first))
			return s;
		else
			return Character.toUpperCase(first) + s.substring(1);
	}
}
