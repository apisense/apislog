APISLOG
=========================== 

APISLOG is a Java exception reporting library compatible with Android.

APISLOG currently supports [Sentry](https://getsentry.com/welcome) reports
based on [raven-java](https://github.com/kencochrane/raven-java) project.

## Basic usage

The APISLog method can be invoked with two different strategies to report logs to a backend server.

###1) Live reporting
 
``` java
// Reports immediately the log entry to the remote server (backendUrl) if a network connection is available.
APISLog.sendNow(backendUrl,exception,Level.ERROR,new Entry[]{new StringEntry("tag","value")});
```

###2) Delayed reporting

``` java
// Saves the log entry in the local cache
APISLog.send(backendUrl,exception,Level.ERROR,new Entry[]{new StringEntry("tag","value")});
```

All log entries stored in the cache can then be uploaded to the backend server by invoking: 

``` java
// Reports all the log entries stored in the cache
APISLog.reportLogs();
```

## Basic configuration

Note that some configurations can be done by setting the following Java properties:

* **Cache folder path**: `System.setProperty("apislog.storage","/path/to/cache");`    
* **Default server URL**: `System.setProperty("apislog.url","https://url/to/server");`  

### Report Uncaught Exception
Uncaught exceptions can be catched using the method `APISLog.catchUncaughtException(true)`. The boolean value specifies if the log must be reported automatically before the application crashes or not. 


## Android usage

On Android, APISLog static instance needs to be configured with an AndroidAPILogContext.
With this context, the *default backend url* value can be inserted in `res/value/string.xml` file or always using java system property:
* `<string name="apislog">http://to/server</string>`
	 

``` java
	// Configures APISLOG android context 
	APISLog.getInstance(new AndroidAPILogContext(androidContext));

	APISLog.send(exception,Level.ERROR,new Entry[]{new StringEntry("tag","value")});
	//...
```



