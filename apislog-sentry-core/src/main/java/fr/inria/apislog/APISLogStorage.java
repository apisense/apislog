/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apislog;

import java.util.List;
import java.util.Map.Entry;

/**
 * Interface with the log storage system.
 * 
 * @author Nicolas Haderer
 *
 */
public interface APISLogStorage {
	List<Entry<String, String>> getAllSentryMessage();

	void save(String messageid, String object);

	void clearAllSentryMessage();

	void deleteSentryMessage(String messageId);
}
