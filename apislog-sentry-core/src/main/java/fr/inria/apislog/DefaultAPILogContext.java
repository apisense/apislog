/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apislog;

import java.util.HashMap;
import java.util.Map;

/**
 * Default implementation of the logging context.
 * 
 * @author Nicolas Haderer
 *
 */
public class DefaultAPILogContext implements APILogContext {
	private String sentryDns = null;

	public DefaultAPILogContext() {
		final String sentryDns = System.getProperty(
				APISLog.PROPERTY_BACKEND_URL, null);
		if (sentryDns != null)
			this.sentryDns = sentryDns;

		final String period = System.getProperty(
				APISLog.PROPERTY_SCHEDULE_PERIOD, null);
		if (period != null) {
			// TODO: Program period scheduler to report all logs
		}
	}

	public String getSentryDns() {
		return this.sentryDns;
	}

	@Override
	public String getLoggerName() {
		return "SentryLogger";
	}

	@Override
	public Boolean hasNetworkConnection() {
		return true;
	}

	@Override
	public Map<String, String> getCommunTags() {
		return new HashMap<String, String>(0);
	}
}