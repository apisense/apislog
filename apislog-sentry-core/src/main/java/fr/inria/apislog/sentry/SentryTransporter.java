/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apislog.sentry;

import java.io.IOException;
import java.util.UUID;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import fr.inria.apislog.APILogMessage;
import fr.inria.apislog.APILogTransporter;
import fr.inria.apislog.APISLog;
import fr.inria.apislog.raven.Events;
import fr.inria.apislog.raven.SentryDsn;
import fr.inria.apislog.raven.Transport;
import fr.inria.apislog.raven.Utils;
import fr.inria.apislog.utils.Base64;

public class SentryTransporter implements APILogTransporter{

	@Override
	public String serialize(APILogMessage log) {

		
		final SentryDsn dsn = SentryDsn.build(log.sentryUrl);

		final JSONObject obj = new JSONObject();
		
		obj.put("dsn", log.sentryUrl);
		String eventId = UUID.randomUUID().toString().replaceAll("-", "");
		if (log.throwable == null){
			obj.put("culprit", log.title);
			Events.message(obj, log.message);
		} else {
			Events.exception(obj, log.throwable);
		}

		String message = log.message;
		if (message == null) {
			message = (log.throwable == null ? null : log.throwable.getMessage());
			message = (message == null ? "" : message);
		}
		
		final String checksum = message+log.title;

		obj.put("event_id", eventId);
		obj.put("platform", "java");
		obj.put("timestamp", log.timestamp);
		obj.put("message", message);
		obj.put("project", dsn.projectId);
		obj.put("level", log.level == null ? "undefined" : log.level);
		obj.put("logger", log.logger == null ? "undefined" : log.logger);
		obj.put("server_name", APISLog.hostname);
		//obj.put("checksum",Base64.encodeToString(checksum.getBytes(), false));

		if (log.tags != null) {
			JSONObject jsonTags = new JSONObject();
			jsonTags.putAll(log.tags);
			obj.put("tags", jsonTags);
		}
		
		return obj.toJSONString();
	}
	
	/*public static String calculateChecksum(String message) {
		byte bytes[] = message.getBytes();
		Checksum checksum = new CRC32();
		checksum.update(bytes, 0, bytes.length);
		return String.valueOf(checksum.getValue());
	}*/

	@Override
	public void send(final String message) throws Exception {
		
		final JSONObject json = (JSONObject) JSONValue.parse(message);
		
		byte[] raw = Utils.toUtf8(json.toJSONString());
		raw = Utils.compress(raw);
		final String encodedString = Base64.encodeToString(raw,false);
		//System.out.println("send "+json.toJSONString());
		final SentryDsn dsn = SentryDsn.build(String.valueOf(json.get("dsn")));
		final Long timespamp = Long.parseLong(json.get("timestamp").toString());
		
		new Transport.Http(dsn).send(encodedString,timespamp);
	}
	
}
