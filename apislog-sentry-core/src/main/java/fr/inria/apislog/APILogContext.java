/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apislog;

import java.util.Map;

/**
 * @author Nicolas Haderer
 * 
 */
public interface APILogContext {

	/**
	 * Returns the URL of the Sentry server.
	 * 
	 * @return
	 */
	String getSentryDns();

	/**
	 * Returns the name of the logger.
	 * 
	 * @return
	 */
	String getLoggerName();

	/**
	 * Returns whether the network is connected or not.
	 * 
	 * @return true if the network is connected.
	 */
	Boolean hasNetworkConnection();

	/**
	 * Returns the list of common tags.
	 * 
	 * @return maps containing key:value tuples.
	 */
	Map<String, String> getCommunTags();
}
