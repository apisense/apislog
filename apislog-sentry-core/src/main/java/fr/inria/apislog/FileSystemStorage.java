/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apislog;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

public class FileSystemStorage implements APISLogStorage {

	private String serializationFolderPath;

	public FileSystemStorage(final String serializationFolderPath) {

		this.serializationFolderPath = serializationFolderPath;

		final File baseFile = new File(serializationFolderPath);
		if (!baseFile.exists()) {
			baseFile.mkdirs();
		}
	}

	@Override
	public List<Entry<String, String>> getAllSentryMessage() {
		final List<Entry<String, String>> messages = new ArrayList<Entry<String, String>>();
		final File serializationFolder = new File(this.serializationFolderPath);

		for (final File file : serializationFolder.listFiles())
			try {
				final String message = convertStreamToString(new FileInputStream(
						file));
				messages.add(new SimpleEntry<String, String>(file.getName(),
						message));
			} catch (Throwable e) {
				e.printStackTrace();
				file.delete();
			}
		return messages;
	}

	public void clearAllSentryMessage() {
		final File serializationFolder = new File(this.serializationFolderPath);

		for (final File file : serializationFolder.listFiles())
			try {
				file.delete();
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

	public static String convertStreamToString(final InputStream is)
			throws Exception {
		final BufferedReader reader = new BufferedReader(new InputStreamReader(
				is));
		final StringBuilder sb = new StringBuilder();
		String line = null;
		while ((line = reader.readLine()) != null)
			sb.append(line);
		return sb.toString();
	}

	@Override
	public void deleteSentryMessage(String messageId) {
		final File file = new File(serializationFolderPath + "/" + messageId);
		if (file.exists())
			file.delete();
	}

	public static String calculateChecksum(String message) {
		byte bytes[] = message.getBytes();
		Checksum checksum = new CRC32();
		checksum.update(bytes, 0, bytes.length);
		return String.valueOf(checksum.getValue());
	}

	@Override
	public void save(String id, String object) {
		try {
			final FileOutputStream file = new FileOutputStream(
					serializationFolderPath + "/" + id);
			file.write(object.getBytes());
			file.flush();
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
