/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apislog.raven;

import org.json.simple.JSONObject;

/**
 * A JSONProcessor is used to modify JSON requests before they are sent to
 * Sentry. It is expected for a JSON processor to be singleton and be
 * thread-safe.
 *
 * To register JSONProcessors, refer to the settings of the appender used.
 *
 * @author vvasabi
 * @since 1.0
 */
public interface JSONProcessor {

    /**
     * This is called when a message is logged. Since
     * {@link #process(JSONObject, Throwable)} may be executed on a different
     * thread, this method should copy any data the processor needs into
     * {@link RavenMDC}.
     *
     * For each message logged, this method should be called exactly once.
     */
    void prepareDiagnosticContext();

    /**
     * This is called after the message logged is processed (in synchronous
     * mode), or after the message has been sent to the processing queue (in
     * asynchronous mode). The intention of this method is to clear the data
     * put in {@link RavenMDC} by {@link #prepareDiagnosticContext()} to prevent
     * memory leak.
     *
     * For each message logged, this method should be called exactly once.
     */
    void clearDiagnosticContext();

    /**
     * Modify the JSON request object specified before it is sent to Sentry.
     * This method may be called concurrently and therefore must be thread-safe.
     *
     * @param json request JSON object to be modified
     * @param exception exception attached with the message and may be null
     */
    void process(JSONObject json, Throwable exception);

}
