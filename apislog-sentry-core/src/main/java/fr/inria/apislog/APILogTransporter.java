/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apislog;

/**
 * Transportation layer for the collected logs.
 * 
 * @author Nicolas Haderer
 * 
 */
public interface APILogTransporter {
	/**
	 * @param sentryMessage
	 * @return
	 */
	String serialize(APILogMessage sentryMessage);

	/**
	 * @param message
	 * @throws Exception
	 */
	void send(String message) throws Exception;
}
