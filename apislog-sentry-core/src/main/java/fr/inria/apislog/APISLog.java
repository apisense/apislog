/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apislog;

import java.lang.Thread.UncaughtExceptionHandler;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fr.inria.apislog.sentry.SentryTransporter;

public class APISLog {
	final public static String FATAL = "fatal";
	final public static String ERROR = "error";
	final public static String WARNING = "warning";
	final public static String INFO = "info";
	final public static String DEBUG = "debug";

	final public static String PROPERTY_BACKEND_URL = "apislog.url";
	final public static String PROPERTY_SCHEDULE_PERIOD = "apislog.period";
	final public static String PROPERTY_STORAGE_PATH = "apislog.storage";

	/* List of authorized log level */
	final public static Set<String> authorizedLevel = new HashSet<String>();

	/* sentry static instance */
	private static APISLog instance;

	public static String hostname = "unavailable";

	public static APISLog getInstance() {
		if (instance == null) {
			final APILogTransporter transporter = new SentryTransporter();
			final APISLogStorage store = new FileSystemStorage(getCachePath());
			final APILogContext context = new DefaultAPILogContext();
			instance = new APISLog(context, transporter, store);
			APISLog.setLogLevelAuthrozation(new String[] { DEBUG, INFO,
					WARNING, ERROR, FATAL });
		}

		return instance;
	}

	public static APISLog getInstance(final APILogContext context) {
		if (instance == null) {
			final APILogTransporter transporter = new SentryTransporter();
			final APISLogStorage store = new FileSystemStorage(getCachePath());
			APISLog.instance = new APISLog(context, transporter, store);
			APISLog.setLogLevelAuthrozation(new String[] { DEBUG, INFO,
					WARNING, ERROR, FATAL });
		}

		return instance;
	}

	private static String getCachePath() {
		String cacheFolder = System.getProperty(PROPERTY_STORAGE_PATH, null);
		if (cacheFolder == null) {
			cacheFolder = System.getProperty("java.io.tmpdir");
		}
		return cacheFolder + "/apislog-cache";
	}

	protected static ExecutorService executor = Executors
			.newScheduledThreadPool(1);

	/* private sentry context */
	private APILogContext logContext;

	private APILogTransporter logTransporter;

	private APISLogStorage logStorage;

	public APISLog(APILogContext sentryContext, APILogTransporter transporter,
			APISLogStorage storage) {
		this.logContext = sentryContext;
		this.logTransporter = transporter;
		this.logStorage = storage;

		executor.submit(new Runnable() {
			public void run() {
				APISLog.hostname = hostname();
			}
		});
	}

	public void clearLogCache() {
		this.logStorage.clearAllSentryMessage();
	}

	public static String hostname() {
		try {
			return InetAddress.getLocalHost().getCanonicalHostName();
		} catch (UnknownHostException e) {
			return "unavailable";
		}
	}

	public static void setLogLevelAuthrozation(String[] authorizedLevel) {
		APISLog.authorizedLevel.removeAll(APISLog.authorizedLevel);
		for (final String level : authorizedLevel) {
			APISLog.authorizedLevel.add(level);
		}
	}

	@SuppressWarnings("unchecked")
	public static void catchUncaughtException(final Boolean reportDirectly) {
		Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler() {
			public void uncaughtException(Thread t, Throwable e) {
				e.printStackTrace();
				if (reportDirectly)
					sendNow(e, FATAL);
				else
					send(e, FATAL);
			}
		});

	}

	private static String getDefaultTagName() {
		final StackTraceElement[] stackTraceElements = Thread.currentThread()
				.getStackTrace();
		final String fullClassName = stackTraceElements[3].getClassName();
		final String className = fullClassName.substring(fullClassName
				.lastIndexOf(".") + 1);
		final String packageName = fullClassName.substring(0,
				fullClassName.lastIndexOf("."));
		int lineNumber = stackTraceElements[3].getLineNumber();

		return packageName + "." + className + "(" + lineNumber + ")";
	}

	public static void send(String message, String level,
			Entry<String, String>... tags) {
		log(null, getDefaultTagName(), message, tags, level, null, null, false,
				false);
	}

	public static void send(String url, String message, String level,
			Entry<String, String>... tags) {
		log(url, getDefaultTagName(), message, tags, level, null, null, false,
				false);
	}

	public static void sendNow(String message, String level,
			Entry<String, String>... tags) {
		log(null, getDefaultTagName(), message, tags, level, null, null, true,
				true);
	}

	public static void sendNow(String url, String message, String level,
			Entry<String, String>... tags) {
		log(url, getDefaultTagName(), message, tags, level, null, null, true,
				true);
	}

	public static void sendNow(Throwable throwable, String level,
			Entry<String, String>... tags) {
		log(null, null, null, tags, level, throwable, null, true, true);
	}

	public static void sendNow(String url, Throwable throwable, String level,
			Entry<String, String>... tags) {
		log(url, null, null, tags, level, throwable, null, true, true);
	}

	public static void send(Throwable throwable, String level,
			Entry<String, String>... tags) {
		log(null, null, null, tags, level, throwable, null, false, false);
	}

	public static void send(String url, Throwable throwable, String level,
			Entry<String, String>... tags) {
		log(url, null, null, tags, level, throwable, null, false, false);
	}

	/**
	 * 
	 * Log sentry message
	 * 
	 * @param title
	 *            Log title
	 * @param message
	 *            Log message
	 * @param tags
	 *            Log tags.
	 * @param level
	 *            Log level. Sentry server consider only log value
	 *            "debug","info","error","fatal"
	 * @param reportImmediately
	 *            Send log on Sentry server directly. Otherwise the log will be
	 *            sent later
	 * @param asynchronous
	 *            Serialize log which will be sent later
	 */
	@SuppressWarnings("static-access")
	public static void log(String sentryDns, final String title,
			final String message, final Entry<String, String>[] tags,
			final String level, final Throwable throwable, String logger,
			boolean reportImmediately, boolean asynchronous) {

		if (sentryDns == null) {
			sentryDns = getInstance().logContext.getSentryDns();
			if (sentryDns == null)
				return;
		}

		if (logger == null)
			logger = getInstance().logContext.getLoggerName();

		if (!getInstance().authorizedLevel.contains(level))
			return;

		// create new sentry message
		final APILogMessage sentryMessage = new APILogMessage(sentryDns, title,
				message, level, throwable, logger);
		sentryMessage.tags.putAll(getInstance().logContext.getCommunTags());

		if (tags != null) {
			for (Entry<String, String> entry : tags) {
				sentryMessage.addTag(entry.getKey(), entry.getValue());
			}
		}

		final String serializedLog = getInstance().logTransporter
				.serialize(sentryMessage);

		if (reportImmediately) {
			if (getInstance().logContext.hasNetworkConnection()) {

				boolean isSend = false;

				try {

					reportLogs(serializedLog, asynchronous);

					isSend = true;

				} catch (Exception e) {

					// TODO check network exception

					// save the message if it cannot be send directly
					getInstance().logStorage.save(
							String.valueOf(sentryMessage.timestamp),
							serializedLog);
				}

				if (isSend) {
					reportLogs();
				}
			} else
				getInstance().logStorage.save(
						String.valueOf(sentryMessage.timestamp), serializedLog);
		} else
			getInstance().logStorage.save(
					String.valueOf(sentryMessage.timestamp), serializedLog);
	}

	public static void reportLogs() {
		reportLogs(true);
	}

	/**
	 * Send all serialized logs to a sentry server
	 * 
	 * @param asynchronous
	 *            True for execute report in a separate server
	 */
	public static void reportLogs(boolean asynchronous) {
		if (getInstance().logContext.hasNetworkConnection())
			for (final Entry<String, String> entry : getInstance().logStorage
					.getAllSentryMessage()) {
				if (asynchronous) {
					reportLogs(entry.getValue(), new Runnable() {
						public void run() {
							getInstance().logStorage.deleteSentryMessage(entry
									.getKey());
						}
					});
				} else {
					try {
						reportLogs(entry.getValue());
					} catch (Exception e) {
						e.printStackTrace();
					}
					getInstance().logStorage
							.deleteSentryMessage(entry.getKey());
				}
			}
	}

	/**
	 * Send a log message to sentry server
	 * 
	 * @param sentryMessage
	 *            log message
	 * @param asynchronous
	 *            True for execute report in a separate server
	 */
	private static void reportLogs(final String sentryMessage,
			boolean asynchronous) throws Exception {
		if (getInstance().logContext.hasNetworkConnection())
			if (asynchronous)
				reportLogs(sentryMessage, null);
			else
				reportLogs(sentryMessage);
	}

	/**
	 * Send message to a sentry server in a separate thread
	 * 
	 * @param message
	 * @param callback
	 *            Called when message are sent
	 */
	private static void reportLogs(final String message, final Runnable callback) {
		executor.submit(new Runnable() {
			public void run() {
				try {
					reportLogs(message);
					if (callback != null)
						callback.run();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Send message to a sentry server
	 * 
	 * @param sentryMessage
	 */
	private static void reportLogs(final String message) throws Exception {
		getInstance().logTransporter.send(message);
	}
}
