/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apislog;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Nicolas Haderer
 *
 */
public class APILogMessage {
	final public String sentryUrl;
	final public String title;
	final public String message;
	final public String logger;
	final public String level;

	final public Throwable throwable;

	final public long timestamp = System.currentTimeMillis();

	final public Map<String, String> tags;

	public APILogMessage(final String dns, final String title,
			final String message, final String level,
			final Throwable exception, final String logger) {
		this.sentryUrl = dns;
		this.title = title;
		this.message = message;
		this.tags = new HashMap<String, String>();
		this.level = level;
		this.throwable = exception;
		this.logger = logger;
	}

	public void addTag(String tagName, String tagValue) {
		this.tags.put(tagName, tagValue);
	}
}
